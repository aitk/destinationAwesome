// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
//
'use strict';
Parse.Cloud.define('getCountry', function(request, response){
  var Countries = Parse.Object.extend('Countries');
  var query = new Parse.Query(Countries);

  console.log('REQUEST', request);

  query.exists('Name');

  query.find({
    success: function(countries) {
      console.log(countries.length);
      if (countries.length  > 0 ) {
        var rand = Math.floor(Math.random() * (countries.length));

        if(Countries[rand]){
          response.success({'country': countries[rand]});
        } else {
          rand = Math.floor(Math.random() * (countries.length));
          response.success({'country': countries[rand]});
        }

      } else {
        response.error({'country':null});
      }
    },
    error: function() {
      response.error({'country': null});
    }
  });
});