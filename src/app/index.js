'use strict';

angular.module('destinationAwesomev2', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngRoute'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      }).when('/about', {
        templateUrl: 'app/about/about.html',
        controller: 'AboutCtrl'
      }).when('/lets-go', {
        templateUrl: 'app/lets-go-there/lets-go-there.html',
        controller: 'LetsGoCtrl'
      }).when('/letsgo', {
        templateUrl: 'app/lets-go-there/lets-go-there.html',
        controller: 'LetsGoCtrl'
      }).when('/letsGo', {
        templateUrl: 'app/lets-go-there/lets-go-there.html',
        controller: 'LetsGoCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
;
