'use strict';

angular.module('destinationAwesomev2')
  .controller('LetsGoCtrl', function ($scope, $routeParams, $http) {
    $scope.country = $routeParams.country;
  });
