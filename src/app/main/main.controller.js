'use strict';

angular.module('destinationAwesomev2')
  .controller('MainCtrl', function ($scope, $http, $location) {
    function init(){
       Parse.initialize('PQytZuoXfHm4IiHejbuFc1n8BK9C8TdaygQdLjOy', '0UZfjnSWtwvacAHJQ7D0AUswynLA0DOlwvi08W3O');

      $scope.africaActive = $scope.asiaActive = $scope.europeActive = $scope.northAmericaActive = $scope.southAmericaActive = $scope.oceaniaActive = true;
    }

    $scope.home = function(){
      $scope.show = false;
      $scope.result = '';
    };

    $scope.letsGo = function(){
      var url = '/lets-go?country=' + $scope.result;
      $location.url(url);
    };

    $scope.randomize = function(){
      $scope.result = '...';
      getCountry(function(err, country){
        if(!err){
          console.log(country);
          $scope.result = country.Name;
          $scope.show = true;
          $scope.$apply();
        } else {
          $scope.result = 'There was an error getting the results';
          $scope.$apply();
        }
      });
    };

    function selectContinents(){
      var continents = [];
      if($scope.africaActive){
        continents.push('Africa');
      }
      if($scope.asiaActive){
        continents.push('Asia');
      }
      if($scope.europeActive){
        continents.push('Europe');
      }
      if($scope.northAmericaActive){
        continents.push('North America');
      }
      if($scope.southAmericaActive){
        continents.push('South America');
      }
      if($scope.oceaniaActive){
        continents.push('Oceania');
      }

      return continents;
    }

    function getCountry(callback){

      var availContinents = selectContinents();
      Parse.Cloud.run('getCountry', { 'continents': availContinents, foo: 'bar'}, {
        success: function (country) {
          console.log('Successfully got a country');
          if(country !== null){
            callback(null, country.country.attributes);
          } else {
            callback( new Error('Could not get country from api'));
          }
        },
        error: function () {
          callback( new Error('Could not get country from api'));
        }
      });
    }

    init();
  });
